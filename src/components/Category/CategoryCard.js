import React from 'react'

const CategoryCard = () => (
  <article className="entry card">
    <div className="entry__img-holder card__img-holder">
      <a href="single-post.html">
        <div className="thumb-container thumb-70">
          <img data-src="/img/content/grid/grid_post_1.jpg" src="/img/empty.png" className="entry__img lazyload" alt />
        </div>
      </a>
      <a
        href="#"
        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
      >
        world
      </a>
    </div>
    <div className="entry__body card__body">
      <div className="entry__header">
        <h2 className="entry__title">
          <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
        </h2>
        <ul className="entry__meta">
          <li className="entry__meta-author">
            <span>by</span>
            <a href="#">DeoThemes</a>
          </li>
          <li className="entry__meta-date">Jan 21, 2018</li>
        </ul>
      </div>
      <div className="entry__excerpt">
        <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
      </div>
    </div>
  </article>
)

export default CategoryCard
