import React from 'react'
import CategoryCard from './CategoryCard'
import SideBar from '../SideBar/SideBar'

const CategorySection = () => (
  <div className="main-container container" id="main-container">
    {}
    <div className="row">
      {}
      <div className="col-lg-8 blog__content mb-72">
        <h1 className="page-title">World</h1>
        <div className="row card-row">
          <div className="col-md-6">
            <CategoryCard />
          </div>
          <div className="col-md-6">
            <CategoryCard />
          </div>
          <div className="col-md-6">
            <CategoryCard />
          </div>
          <div className="col-md-6">
            <CategoryCard />
          </div>
        </div>
        {}
      </div>{' '}
      {}
      {}
      <SideBar /> {}
    </div>{' '}
    {}
  </div>
)

export default CategorySection
