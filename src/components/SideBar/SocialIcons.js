import React from "react";

const SocialIcons = () => (
  <aside className="widget widget-socials">
    <h4 className="widget-title">Let's hang out on social</h4>
    <div className="socials socials--wide socials--large">
      <div className="row row-16">
        <div className="col">
          <a
            className="social social-facebook"
            href="#"
            title="facebook"
            target="_blank"
            aria-label="facebook"
          >
            <i className="ui-facebook" />
            <span className="social__text">Facebook</span>
          </a>
          {}
          <a
            className="social social-twitter"
            href="#"
            title="twitter"
            target="_blank"
            aria-label="twitter"
          >
            <i className="ui-twitter" />
            <span className="social__text">Twitter</span>
          </a>
          {}
          <a
            className="social social-youtube"
            href="#"
            title="youtube"
            target="_blank"
            aria-label="youtube"
          >
            <i className="ui-youtube" />
            <span className="social__text">Youtube</span>
          </a>
        </div>
        <div className="col">
          <a
            className="social social-google-plus"
            href="#"
            title="google"
            target="_blank"
            aria-label="google"
          >
            <i className="ui-google" />
            <span className="social__text">Google+</span>
          </a>
          {}
          <a
            className="social social-instagram"
            href="#"
            title="instagram"
            target="_blank"
            aria-label="instagram"
          >
            <i className="ui-instagram" />
            <span className="social__text">Instagram</span>
          </a>
          {}
          <a
            className="social social-rss"
            href="#"
            title="rss"
            target="_blank"
            aria-label="rss"
          >
            <i className="ui-rss" />
            <span className="social__text">Rss</span>
          </a>
        </div>
      </div>
    </div>
  </aside>
);

export default SocialIcons;
