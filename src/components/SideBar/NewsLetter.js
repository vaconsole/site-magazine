import React from "react";

const NewsLetter = () => (
  <aside className="widget widget_mc4wp_form_widget">
    <h4 className="widget-title">Newsletter</h4>
    <p className="newsletter__text">
      <i className="ui-email newsletter__icon" />
      Subscribe for our daily news
    </p>
    <form className="mc4wp-form" method="post">
      <div className="mc4wp-form-fields">
        <div className="form-group">
          <input type="email" name="EMAIL" placeholder="Your email" required />
        </div>
        <div className="form-group">
          <input
            type="submit"
            className="btn btn-lg btn-color"
            defaultValue="Sign Up"
          />
        </div>
      </div>
    </form>
  </aside>
);

export default NewsLetter;
