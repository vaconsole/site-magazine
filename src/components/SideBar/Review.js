import React from "react";

const Review = () => (
  <aside className="widget widget-review-posts">
    <h4 className="widget-title">Game Reviews</h4>
    <article className="entry">
      <div className="entry__img-holder">
        <a href="single-post-games.html">
          <div className="thumb-container thumb-60">
            <img
              data-src="img/content/review/review_post_3.jpg"
              src="img/empty.png"
              className="entry__img lazyload"
              alt
            />
            <span className="entry-score">9.2</span>
          </div>
        </a>
      </div>
      <div className="entry__body">
        <div className="entry__header">
          <h2 className="entry__title">
            <a href="single-post-games.html">
              Deep Rock Galactic Is A Cooperative Blast
            </a>
          </h2>
          <ul className="entry__meta">
            <li className="entry__meta-author">
              <span>by</span>
              <a href="#">DeoThemes</a>
            </li>
            <li className="entry__meta-date">Jan 21, 2018</li>
          </ul>
        </div>
      </div>
    </article>
    <article className="entry">
      <div className="entry__img-holder">
        <a href="single-post-games.html">
          <div className="thumb-container thumb-60">
            <img
              data-src="img/content/review/review_post_4.jpg"
              src="img/empty.png"
              className="entry__img lazyload"
              alt
            />
            <span className="entry-score">8.9</span>
          </div>
        </a>
      </div>
      <div className="entry__body">
        <div className="entry__header">
          <h2 className="entry__title">
            <a href="single-post-games.html">
              With a mix of dynamic stealth and high-flying action, Spider-Man
            </a>
          </h2>
          <ul className="entry__meta">
            <li className="entry__meta-author">
              <span>by</span>
              <a href="#">DeoThemes</a>
            </li>
            <li className="entry__meta-date">Jan 21, 2018</li>
          </ul>
        </div>
      </div>
    </article>
    <article className="entry">
      <div className="entry__img-holder">
        <a href="single-post-games.html">
          <div className="thumb-container thumb-60">
            <img
              data-src="img/content/review/review_post_5.jpg"
              src="img/empty.png"
              className="entry__img lazyload"
              alt
            />
            <span className="entry-score">8.5</span>
          </div>
        </a>
      </div>
      <div className="entry__body">
        <div className="entry__header">
          <h2 className="entry__title">
            <a href="single-post-games.html">
              With a mix of dynamic stealth and high-flying action, Spider-Man
            </a>
          </h2>
          <ul className="entry__meta">
            <li className="entry__meta-author">
              <span>by</span>
              <a href="#">DeoThemes</a>
            </li>
            <li className="entry__meta-date">Jan 21, 2018</li>
          </ul>
        </div>
      </div>
    </article>
    <button className="btn btn-lg btn-light btn-wide load-more">
      <span>
        Load More <i className="ui-arrow-down" />
      </span>
    </button>
  </aside>
);

export default Review;
