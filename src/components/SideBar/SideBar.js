import React from 'react'
import PopularPost from './PopularPost'
import NewsLetter from './NewsLetter'
import SocialIcons from './SocialIcons'

const SideBar = ({ children }) => <aside className="col-lg-4 sidebar sidebar--right">{children}</aside>

export default SideBar
