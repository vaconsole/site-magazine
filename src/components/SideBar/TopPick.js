import React from "react";

const TopPick = () => (
  <aside className="widget widget-popular-posts">
    <h4 className="widget-title">Top Picks</h4>
    <ul className="post-list-small">
      <li className="post-list-small__item">
        <article className="post-list-small__entry clearfix">
          <div className="post-list-small__img-holder">
            <div className="thumb-container thumb-100">
              <a href="single-post-games.html">
                <img
                  data-src="img/content/post_small/post_small_37.jpg"
                  src="img/empty.png"
                  alt
                  className="post-list-small__img--rounded lazyload"
                />
              </a>
            </div>
          </div>
          <div className="post-list-small__body">
            <h3 className="post-list-small__entry-title">
              <a href="single-post-games.html">
                Follow These Smartphone Habits of Successful Entrepreneurs
              </a>
            </h3>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
        </article>
      </li>
      <li className="post-list-small__item">
        <article className="post-list-small__entry clearfix">
          <div className="post-list-small__img-holder">
            <div className="thumb-container thumb-100">
              <a href="single-post-games.html">
                <img
                  data-src="img/content/post_small/post_small_38.jpg"
                  src="img/empty.png"
                  alt
                  className="post-list-small__img--rounded lazyload"
                />
              </a>
            </div>
          </div>
          <div className="post-list-small__body">
            <h3 className="post-list-small__entry-title">
              <a href="single-post-games.html">
                Lose These 12 Bad Habits If You're Serious About Becoming a
                Millionaire
              </a>
            </h3>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
        </article>
      </li>
      <li className="post-list-small__item">
        <article className="post-list-small__entry clearfix">
          <div className="post-list-small__img-holder">
            <div className="thumb-container thumb-100">
              <a href="single-post-games.html">
                <img
                  data-src="img/content/post_small/post_small_39.jpg"
                  src="img/empty.png"
                  alt
                  className="post-list-small__img--rounded lazyload"
                />
              </a>
            </div>
          </div>
          <div className="post-list-small__body">
            <h3 className="post-list-small__entry-title">
              <a href="single-post-games.html">
                June in Africa: Taxi wars, smarter cities and increased
                investments
              </a>
            </h3>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
        </article>
      </li>
      <li className="post-list-small__item">
        <article className="post-list-small__entry clearfix">
          <div className="post-list-small__img-holder">
            <div className="thumb-container thumb-100">
              <a href="single-post-games.html">
                <img
                  data-src="img/content/post_small/post_small_40.jpg"
                  src="img/empty.png"
                  alt
                  className="post-list-small__img--rounded lazyload"
                />
              </a>
            </div>
          </div>
          <div className="post-list-small__body">
            <h3 className="post-list-small__entry-title">
              <a href="single-post-games.html">
                PUBG Desert Map Finally Revealed, Here Are All The Details
              </a>
            </h3>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
        </article>
      </li>
    </ul>
  </aside>
);

export default TopPick;
