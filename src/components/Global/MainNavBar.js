import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'
import Navbar from './Navbar'

const MainNavBar = () => {
  const {
    site: {
      siteMetadata: { mainMenu, short_name },
    },
    logo,
  } = useStaticQuery(graphql`
    query HeaderQuery {
      logo: file(name: { eq: "site-icon" }) {
        childImageSharp {
          fixed(height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }

      site {
        siteMetadata {
          short_name
          mainMenu {
            name
            link
          }
        }
      }
    }
  `)
  const logoFixed = logo.childImageSharp.fixed
  return (
    <Navbar menu={mainMenu}>{logoFixed ? <Img fixed={logoFixed} alt={short_name} /> : <h2> {short_name}</h2>}</Navbar>
  )
}

export default MainNavBar
