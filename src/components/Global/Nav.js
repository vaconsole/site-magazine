import React from 'react'

const Nav = () => (
  <header className="nav">
    <div className="nav__holder nav--sticky">
      <div className="container relative">
        <div className="flex-parent">
          {}
          <button className="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open side menu">
            <span className="nav-icon-toggle__box">
              <span className="nav-icon-toggle__inner" />
            </span>
          </button>
          {}
          <a href="index.html" className="logo">
            {/* <img
              className="logo__img"
              src="img/logo_default.png"
              srcSet="img/logo_default.png 1x, img/logo_default@2x.png 2x"
              alt="logo"
            /> */}
            <h2>Viral News </h2>
          </a>
          {}
          <nav className="flex-child nav__wrap d-none d-lg-block">
            <ul className="nav__menu">
              <li className="nav__dropdown active">
                <a href="index.html">Home</a>
                <ul className="nav__dropdown-menu">
                  <li>
                    <a href="index.html">Home Default</a>
                  </li>
                  <li>
                    <a href="index-politics.html">Home Politics</a>
                  </li>
                  <li>
                    <a href="index-fashion.html">Home Fashion</a>
                  </li>
                  <li>
                    <a href="index-games.html">Home Games</a>
                  </li>
                  <li>
                    <a href="index-videos.html">Home Videos</a>
                  </li>
                  <li>
                    <a href="index-music.html">Home Music</a>
                  </li>
                </ul>
              </li>
              <li className="nav__dropdown">
                <a href="#">Posts</a>
                <ul className="nav__dropdown-menu nav__megamenu">
                  <li>
                    <div className="nav__megamenu-wrap">
                      <div className="row">
                        <div className="col nav__megamenu-item">
                          <article className="entry">
                            <div className="entry__img-holder">
                              <a href="single-post.html">
                                <img src="img/content/grid/grid_post_1.jpg" alt className="entry__img" />
                              </a>
                              <a
                                href="categories.html"
                                className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                              >
                                world
                              </a>
                            </div>
                            <div className="entry__body">
                              <h2 className="entry__title">
                                <a href="single-post.html">
                                  Follow These Smartphone Habits of Successful Entrepreneurs
                                </a>
                              </h2>
                            </div>
                          </article>
                        </div>
                        <div className="col nav__megamenu-item">
                          <article className="entry">
                            <div className="entry__img-holder">
                              <a href="single-post.html">
                                <img src="img/content/grid/grid_post_2.jpg" alt className="entry__img" />
                              </a>
                              <a
                                href="categories.html"
                                className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                              >
                                fashion
                              </a>
                            </div>
                            <div className="entry__body">
                              <h2 className="entry__title">
                                <a href="single-post.html">
                                  3 Things You Can Do to Get Your Customers Talking About Your Business
                                </a>
                              </h2>
                            </div>
                          </article>
                        </div>
                        <div className="col nav__megamenu-item">
                          <article className="entry">
                            <div className="entry__img-holder">
                              <a href="single-post.html">
                                <img src="img/content/grid/grid_post_3.jpg" alt className="entry__img" />
                              </a>
                              <a
                                href="categories.html"
                                className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"
                              >
                                business
                              </a>
                            </div>
                            <div className="entry__body">
                              <h2 className="entry__title">
                                <a href="single-post.html">
                                  Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire
                                </a>
                              </h2>
                            </div>
                          </article>
                        </div>
                        <div className="col nav__megamenu-item">
                          <article className="entry">
                            <div className="entry__img-holder">
                              <a href="single-post.html">
                                <img src="img/content/grid/grid_post_4.jpg" alt className="entry__img" />
                              </a>
                              <a
                                href="categories.html"
                                className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                              >
                                lifestyle
                              </a>
                            </div>
                            <div className="entry__body">
                              <h2 className="entry__title">
                                <a href="single-post.html">
                                  10 Horrible Habits You're Doing Right Now That Are Draining Your Energy
                                </a>
                              </h2>
                            </div>
                          </article>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>{' '}
                {}
              </li>
              <li className="nav__dropdown">
                <a href="#">Pages</a>
                <ul className="nav__dropdown-menu">
                  <li>
                    <a href="about.html">About</a>
                  </li>
                  <li>
                    <a href="contact.html">Contact</a>
                  </li>
                  <li>
                    <a href="search-results.html">Search Results</a>
                  </li>
                  <li>
                    <a href="categories.html">Categories</a>
                  </li>
                  <li>
                    <a href="404.html">404</a>
                  </li>
                </ul>
              </li>
              <li className="nav__dropdown">
                <a href="#">Features</a>
                <ul className="nav__dropdown-menu">
                  <li>
                    <a href="shortcodes.html">Shortcodes</a>
                  </li>
                  <li className="nav__dropdown">
                    <a href="#">Single Post</a>
                    <ul className="nav__dropdown-menu">
                      <li>
                        <a href="single-post.html">Style 1</a>
                      </li>
                      <li>
                        <a href="single-post-politics.html">Style 2</a>
                      </li>
                      <li>
                        <a href="single-post-fashion.html">Style 3</a>
                      </li>
                      <li>
                        <a href="single-post-games.html">Style 4</a>
                      </li>
                      <li>
                        <a href="single-post-videos.html">Style 5</a>
                      </li>
                      <li>
                        <a href="single-post-music.html">Style 6</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Purchase</a>
              </li>
            </ul>{' '}
            {}
          </nav>{' '}
          {}
          {}
          <div className="nav__right">
            {}
            <div className="nav__right-item nav__search">
              <a href="#" className="nav__search-trigger" id="nav__search-trigger">
                <i className="ui-search nav__search-trigger-icon" />
              </a>
              <div className="nav__search-box" id="nav__search-box">
                <form className="nav__search-form">
                  <input type="text" placeholder="Search an article" className="nav__search-input" />
                  <button type="submit" className="search-button btn btn-lg btn-color btn-button">
                    <i className="ui-search nav__search-icon" />
                  </button>
                </form>
              </div>
            </div>
          </div>{' '}
          {}
        </div>{' '}
        {}
      </div>{' '}
      {}
    </div>
  </header>
)

export default Nav
