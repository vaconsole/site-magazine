import React from 'react'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import { Link } from 'gatsby'
import styled from '@emotion/styled'
import { css } from '@emotion/core'

const NavLink = styled(Link)`
  background-color: transparent !important;
  box-sizing: border-box !important;
  cursor: pointer !important;
  display: inline-block;
  font-family: 'Open Sans', sans-serif !important;
  font-size: 16px !important;
  font-weight: 500 !important;
  padding: 30px 0 !important;
  text-decoration: none !important;
  touch-action: manipulation !important;
  transition-delay: 0s;
  transition-duration: 0.3s;
  transition-property: all;
  transition-timing-function: ease;
  box-sizing: border-box;
  display: inline-block;
  margin-right: 58px;
  position: relative;

  :hover {
    color: #02a9c0 !important;
    text-decoration: underline !important;
  }
`

export default function TopNavBar({ menu, children, className }) {
  return (
    <Navbar expand="sm" className={`${className}`}>
      <div className="container">
        <Navbar.Brand>
          <Link to="/">{children}</Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto">
            {menu &&
              menu.map(item => {
                return Array.isArray(item.link) ? (
                  <NavDropdown title={item.name} className="text-capitalize">
                    {item.link.map(subItem => (
                      <NavLink to={subItem.link} key={subItem.name} className="dropdown-item text-capitalize">
                        {subItem.name}
                      </NavLink>
                    ))}
                  </NavDropdown>
                ) : (
                  <li key={item.name} className="nav-item">
                    <NavLink to={item.link} className="nav-link text-capitalize">
                      {item.name}
                    </NavLink>
                  </li>
                )
              })}
          </Nav>
        </Navbar.Collapse>
      </div>
    </Navbar>
  )
}
