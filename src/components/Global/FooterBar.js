import React from 'react'
import { Navbar } from 'react-bootstrap'
import { Link, useStaticQuery, graphql } from 'gatsby'

const Footer = () => {
    const {
        site: { siteMetadata },
    } = useStaticQuery(graphql`
    query FooterQuery {
      site {
        siteMetadata {
          short_name
          footerMenu {
            name
            link
          }
        }
      }
    }
  `)

    return (
        <Navbar bg="dark" fixed="bottom" className="text-light mt-5">
            <div className="container">
                © {siteMetadata.short_name}, {new Date().getFullYear()}
                <ul className="d-flex list-unstyled">
                    {siteMetadata.footerMenu.map(item => (
                        <li key={item.name} className="nav-item  pl-3 text-capitalize">
                            <Link to={item.link}>{item.name}</Link>
                        </li>
                    ))}
                </ul>
            </div>
        </Navbar>
    )
}

export default Footer
