import React from 'react'
import PropTypes from 'prop-types'
import TopBar from './TopBar'
import Nav from './Nav'
import Footer from './Footer'
import Navbar from './Navbar'

const Layout = ({ children }) => {
  return (
    <div>
      <main className="main oh" id="main">
        <TopBar /> {}
        {}
        <Nav /> {}
        {children}
        <Footer />
      </main>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}
export default Layout
