import React from "react";

const BreadCrumbs = () => (
  <div className="container">
    <ul className="breadcrumbs">
      <li className="breadcrumbs__item">
        <a href="index.html" className="breadcrumbs__url">
          Home
        </a>
      </li>
      <li className="breadcrumbs__item">
        <a href="index.html" className="breadcrumbs__url">
          News
        </a>
      </li>
      <li className="breadcrumbs__item breadcrumbs__item--current">World</li>
    </ul>
  </div>
);

export default BreadCrumbs;
