import React from 'react'
import BackgroundImage from 'gatsby-background-image'

// /img/content/single/single_post_games_featured_img.jpg
const BannerImage = ({ img }) => (
  <div className="thumb thumb--size-6">
    <BackgroundImage className="entry__img-holder thumb__img-holder" fluid={img} backgroundColor="#040e18" />
  </div>
)

export default BannerImage
