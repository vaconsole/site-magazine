import React from 'react'
import { Link } from 'gatsby'
import SocialShare from './SocialShare'
import FinalReview from './FinalReview'
import Tags from './Tags'
import NewsLetterWide from './NewsLetterWide'
import PreviousNextPost from './PreviousNextPost'
import Author from './Author'
import RelatedArticle from './RelatedArticle'
import Comments from './Comments'
import Reply from './Reply'

const BlogContent = ({ title, contentHtml, categories, tags }) => (
  <div className="col-lg-8 blog__content mb-72">
    <div className="content-box content-box--top-offset">
      {}
      <article className="entry mb-0">
        <div className="single-post__entry-header entry__header">
          <ul className="entry__meta">
            {categories &&
              categories.map(category => (
                <Link className="entry__meta-category" key={category} to={`/category/${category}`}>
                  {category}
                </Link>
              ))}
          </ul>
          <h1 className="single-post__entry-title">{title}</h1>
          <div className="entry__meta-holder">
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
            </ul>
            <ul className="entry__meta">
              <li className="entry__meta-views">
                <i className="ui-eye" />
                <span>1356</span>
              </li>
              <li className="entry__meta-comments">
                <a href="#">
                  <i className="ui-chat-empty" />
                  13
                </a>
              </li>
            </ul>
          </div>
        </div>{' '}
        {}
        <div className="entry__article-wrap">
          {}
          <SocialShare /> {}
          <div className="entry__article">
            <div dangerouslySetInnerHTML={{ __html: contentHtml }} />
            <FinalReview /> {}
            {}
            <div className="entry__tags">
              <i className="ui-tags" />
              <span className="entry__tags-label">Tags:</span>
              {tags &&
                tags.map(tag => (
                  <Link key={tag} to={`/tag/${tag}`}>
                    {tag}
                  </Link>
                ))}
            </div>{' '}
          </div>{' '}
          {}
        </div>{' '}
        {}
        {}
        <NewsLetterWide /> {}
        {}
        <PreviousNextPost />
        {}
        <Author />
        {}
        <RelatedArticle /> {}
      </article>{' '}
      {}
      {}
      <Comments /> {}
      {}
      <Reply /> {}
    </div>{' '}
    {}
  </div>
)

export default BlogContent
