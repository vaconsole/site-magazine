import React from 'react'
import BlogContent from './BlogContent'
import SideBar from '../SideBar/SideBar'
import PopularPost from '../SideBar/PopularPost'
import NewsLetter from '../SideBar/NewsLetter'
import SocialIcons from '../SideBar/SocialIcons'
import Review from '../SideBar/Review'

const BlogSection = () => (
  <div className="main-container container" id="main-container">
    {}
    <div className="row">
      {}
      <BlogContent /> {}
      {}
      <SideBar>
        <Review /> {}
        {}
        <NewsLetter /> {}
        {}
        <SocialIcons /> {}
      </SideBar>
    </div>{' '}
    {}
  </div>
)

export default BlogSection
