import React from "react";

const FinalReview = () => (
  <div
    className="final-review"
    style={{
      backgroundImage: 'url("/img/content/single/final_review.jpg")'
    }}
  >
    <div className="final-review__score">
      <span className="final-review__score-number">9.2</span>
    </div>
    <div className="final-review__text-holder">
      <h6 className="final-review__title">Great</h6>
      <p className="final-review__text">
        Lovingly rendered real-world space tech,playing through actual missions
        is a special thrill,scoring system gives much needed additional
        incentive to perfect your designs
      </p>
    </div>
  </div>
);

export default FinalReview;
