import React from "react";

const PreviousNextPost = () => (
  <nav className="entry-navigation">
    <div className="clearfix">
      <div className="entry-navigation--left">
        <i className="ui-arrow-left" />
        <span className="entry-navigation__label">Previous Post</span>
        <div className="entry-navigation__link">
          <a href="#" rel="next">
            How to design your first mobile app
          </a>
        </div>
      </div>
      <div className="entry-navigation--right">
        <span className="entry-navigation__label">Next Post</span>
        <i className="ui-arrow-right" />
        <div className="entry-navigation__link">
          <a href="#" rel="prev">
            Video Youtube format post. Made with WordPress
          </a>
        </div>
      </div>
    </div>
  </nav>
);

export default PreviousNextPost;
