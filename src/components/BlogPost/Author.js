import React from "react";

const Author = () => (
  <div className="entry-author clearfix">
    <img
      alt
      data-src="img/content/single/author.jpg"
      src="img/empty.png"
      className="avatar lazyload"
    />
    <div className="entry-author__info">
      <h6 className="entry-author__name">
        <a href="#">John Carpet</a>
      </h6>
      <p className="mb-0">
        But unfortunately for most of us our role as gardener has never been
        explained to us. And in misunderstanding our role, we have allowed seeds
        of all types, both good and bad, to enter our inner garden.
      </p>
    </div>
  </div>
);

export default Author;
