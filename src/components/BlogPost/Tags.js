import React from 'react'

const Tags = ({ tags }) => (
  <div className="entry__tags">
    <i className="ui-tags" />
    <span className="entry__tags-label">Tags:</span>
    <a href="#" rel="tag">
      mobile
    </a>
    <a href="#" rel="tag">
      gadgets
    </a>
    <a href="#" rel="tag">
      satelite
    </a>
  </div>
)

export default Tags
