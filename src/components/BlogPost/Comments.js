import React from "react";

const Comments = () => (
  <div className="entry-comments">
    <div className="title-wrap title-wrap--line">
      <h3 className="section-title">3 comments</h3>
    </div>
    <ul className="comment-list">
      <li className="comment">
        <div className="comment-body">
          <div className="comment-avatar">
            <img alt src="img/content/single/comment_1.jpg" />
          </div>
          <div className="comment-text">
            <h6 className="comment-author">Joeby Ragpa</h6>
            <div className="comment-metadata">
              <a href="#" className="comment-date">
                July 17, 2017 at 12:48 pm
              </a>
            </div>
            <p>
              This template is so awesome. I didn’t expect so many features
              inside. E-commerce pages are very useful, you can launch your
              online store in few seconds. I will rate 5 stars.
            </p>
            <a href="#" className="comment-reply">
              Reply
            </a>
          </div>
        </div>
        <ul className="children">
          <li className="comment">
            <div className="comment-body">
              <div className="comment-avatar">
                <img alt src="img/content/single/comment_2.jpg" />
              </div>
              <div className="comment-text">
                <h6 className="comment-author">Alexander Samokhin</h6>
                <div className="comment-metadata">
                  <a href="#" className="comment-date">
                    July 17, 2017 at 12:48 pm
                  </a>
                </div>
                <p>
                  This template is so awesome. I didn’t expect so many features
                  inside. E-commerce pages are very useful, you can launch your
                  online store in few seconds. I will rate 5 stars.
                </p>
                <a href="#" className="comment-reply">
                  Reply
                </a>
              </div>
            </div>
          </li>{" "}
          {}
        </ul>
      </li>{" "}
      {}
      <li>
        <div className="comment-body">
          <div className="comment-avatar">
            <img alt src="img/content/single/comment_3.jpg" />
          </div>
          <div className="comment-text">
            <h6 className="comment-author">Chris Root</h6>
            <div className="comment-metadata">
              <a href="#" className="comment-date">
                July 17, 2017 at 12:48 pm
              </a>
            </div>
            <p>
              This template is so awesome. I didn’t expect so many features
              inside. E-commerce pages are very useful, you can launch your
              online store in few seconds. I will rate 5 stars.
            </p>
            <a href="#" className="comment-reply">
              Reply
            </a>
          </div>
        </div>
      </li>{" "}
      {}
    </ul>
  </div>
);

export default Comments;
