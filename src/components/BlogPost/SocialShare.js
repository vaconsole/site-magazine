import React from "react";

const SocialShare = () => (
  <div className="entry__share">
    <div className="sticky-col">
      <div className="socials socials--rounded socials--large">
        <a
          className="social social-facebook"
          href="#"
          title="facebook"
          target="_blank"
          aria-label="facebook"
        >
          <i className="ui-facebook" />
        </a>
        <a
          className="social social-twitter"
          href="#"
          title="twitter"
          target="_blank"
          aria-label="twitter"
        >
          <i className="ui-twitter" />
        </a>
        <a
          className="social social-google-plus"
          href="#"
          title="google"
          target="_blank"
          aria-label="google"
        >
          <i className="ui-google" />
        </a>
        <a
          className="social social-pinterest"
          href="#"
          title="pinterest"
          target="_blank"
          aria-label="pinterest"
        >
          <i className="ui-pinterest" />
        </a>
      </div>
    </div>
  </div>
);

export default SocialShare;
