import React from "react";

const NewsLetterWide = () => (
  <div className="newsletter-wide">
    <div className="widget widget_mc4wp_form_widget">
      <div className="newsletter-wide__container">
        <div className="newsletter-wide__text-holder">
          <p className="newsletter-wide__text">
            <i className="ui-email newsletter__icon" />
            Subscribe for our daily news
          </p>
        </div>
        <div className="newsletter-wide__form">
          <form className="mc4wp-form" method="post">
            <div className="mc4wp-form-fields">
              <div className="form-group">
                <input
                  type="email"
                  name="EMAIL"
                  placeholder="Your email"
                  required
                />
              </div>
              <div className="form-group">
                <input
                  type="submit"
                  className="btn btn-lg btn-color"
                  defaultValue="Sign Up"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
);

export default NewsLetterWide;
