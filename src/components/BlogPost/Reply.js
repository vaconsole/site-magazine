import React from "react";

const Reply = () => (
  <div id="respond" className="comment-respond">
    <div className="title-wrap">
      <h5 className="comment-respond__title section-title">Leave a Reply</h5>
    </div>
    <form id="form" className="comment-form" method="post" action="#">
      <p className="comment-form-comment">
        <label htmlFor="comment">Comment</label>
        <textarea
          id="comment"
          name="comment"
          rows={5}
          required="required"
          defaultValue={""}
        />
      </p>
      <div className="row row-20">
        <div className="col-lg-4">
          <label htmlFor="name">Name: *</label>
          <input name="name" id="name" type="text" />
        </div>
        <div className="col-lg-4">
          <label htmlFor="comment">Email: *</label>
          <input name="email" id="email" type="email" />
        </div>
        <div className="col-lg-4">
          <label htmlFor="comment">Website:</label>
          <input name="website" id="website" type="text" />
        </div>
      </div>
      <p className="comment-form-submit">
        <input
          type="submit"
          className="btn btn-lg btn-color btn-button"
          defaultValue="Post Comment"
          id="submit-message"
        />
      </p>
    </form>
  </div>
);

export default Reply;
