import React from "react";

const Error404 = () => (
  <div className="main-container container pt-80 pb-80" id="main-container">
    {}
    <div className="blog__content mb-72">
      <div className="container text-center">
        <h1 className="page-404-number">404</h1>
        <h2>Page not found</h2>
        <p>
          Don't fret! Let's get you back on track. Perhaps searching can help
        </p>
        <div className="row justify-content-center mt-40">
          <div className="col-md-6">
            {}
            <form role="search" method="get" className="search-form relative">
              <input
                type="search"
                className="widget-search-input mb-0"
                placeholder="Search an article"
              />
              <button
                type="submit"
                className="widget-search-button btn btn-color"
              >
                <i className="ui-search widget-search-icon" />
              </button>
            </form>
            {}
            <a href="index.html" className="btn btn-lg btn-light mt-40">
              <span>Back to Home</span>
            </a>
          </div>
        </div>{" "}
        {}
      </div>{" "}
      {}
    </div>{" "}
    {}
  </div>
);

export default Error404;
