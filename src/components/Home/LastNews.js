import React from "react";
import SideBarRight from "./SideBarRight";
import AdBanner from "./AdBanner";
import CarouselPosts from "./CarouselPosts";
import PostCategory from "./PostCategory";
import VideoPost from "./VideoPost";
import WorldWidePost from "./WorldWidePost";

const LastNews = () => (
  <div className="main-container container pt-24" id="main-container">
    {}
    <div className="row">
      {}
      <div className="col-lg-8 blog__content">
        {}
        <section className="section tab-post mb-16">
          <div className="title-wrap title-wrap--line">
            <h3 className="section-title">Latest News</h3>
            <div className="tabs tab-post__tabs">
              <ul className="tabs__list">
                <li className="tabs__item tabs__item--active">
                  <a href="#tab-all" className="tabs__trigger">
                    All
                  </a>
                </li>
                <li className="tabs__item">
                  <a href="#tab-world" className="tabs__trigger">
                    World
                  </a>
                </li>
                <li className="tabs__item">
                  <a href="#tab-lifestyle" className="tabs__trigger">
                    Lifestyle
                  </a>
                </li>
                <li className="tabs__item">
                  <a href="#tab-business" className="tabs__trigger">
                    Business
                  </a>
                </li>
                <li className="tabs__item">
                  <a href="#tab-fashion" className="tabs__trigger">
                    Fashion
                  </a>
                </li>
              </ul>{" "}
              {}
            </div>
          </div>
          {}
          <div className="tabs__content tabs__content-trigger tab-post__tabs-content">
            <div
              className="tabs__content-pane tabs__content-pane--active"
              id="tab-all"
            >
              <div className="row card-row">
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_1.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                      >
                        world
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Follow These Smartphone Habits of Successful
                            Entrepreneurs
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_2.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                      >
                        fashion
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            3 Things You Can Do to Get Your Customers Talking
                            About Your Business
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_3.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
                      >
                        mining
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Lose These 12 Bad Habits If You're Serious About
                            Becoming a Millionaire
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_4.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                      >
                        lifestyle
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            10 Horrible Habits You're Doing Right Now That Are
                            Draining Your Energy
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>{" "}
            {}
            <div className="tabs__content-pane" id="tab-world">
              <div className="row card-row">
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_3.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
                      >
                        mining
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Lose These 12 Bad Habits If You're Serious About
                            Becoming a Millionaire
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_4.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                      >
                        lifestyle
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            10 Horrible Habits You're Doing Right Now That Are
                            Draining Your Energy
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_1.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                      >
                        world
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Follow These Smartphone Habits of Successful
                            Entrepreneurs
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_2.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                      >
                        fashion
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            3 Things You Can Do to Get Your Customers Talking
                            About Your Business
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>{" "}
            {}
            <div className="tabs__content-pane" id="tab-lifestyle">
              <div className="row card-row">
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_1.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                      >
                        world
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Follow These Smartphone Habits of Successful
                            Entrepreneurs
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_2.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                      >
                        fashion
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            3 Things You Can Do to Get Your Customers Talking
                            About Your Business
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_3.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
                      >
                        mining
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Lose These 12 Bad Habits If You're Serious About
                            Becoming a Millionaire
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_4.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                      >
                        lifestyle
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            10 Horrible Habits You're Doing Right Now That Are
                            Draining Your Energy
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>{" "}
            {}
            <div className="tabs__content-pane" id="tab-business">
              <div className="row card-row">
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_3.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
                      >
                        mining
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Lose These 12 Bad Habits If You're Serious About
                            Becoming a Millionaire
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_4.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                      >
                        lifestyle
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            10 Horrible Habits You're Doing Right Now That Are
                            Draining Your Energy
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_1.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                      >
                        world
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Follow These Smartphone Habits of Successful
                            Entrepreneurs
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_2.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                      >
                        fashion
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            3 Things You Can Do to Get Your Customers Talking
                            About Your Business
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>{" "}
            {}
            <div className="tabs__content-pane" id="tab-fashion">
              <div className="row card-row">
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_1.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                      >
                        world
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Follow These Smartphone Habits of Successful
                            Entrepreneurs
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_2.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                      >
                        fashion
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            3 Things You Can Do to Get Your Customers Talking
                            About Your Business
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_3.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
                      >
                        mining
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            Lose These 12 Bad Habits If You're Serious About
                            Becoming a Millionaire
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
                <div className="col-md-6">
                  <article className="entry card">
                    <div className="entry__img-holder card__img-holder">
                      <a href="single-post.html">
                        <div className="thumb-container thumb-70">
                          <img
                            data-src="img/content/grid/grid_post_4.jpg"
                            src="img/empty.png"
                            className="entry__img lazyload"
                            alt
                          />
                        </div>
                      </a>
                      <a
                        href="#"
                        className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                      >
                        lifestyle
                      </a>
                    </div>
                    <div className="entry__body card__body">
                      <div className="entry__header">
                        <h2 className="entry__title">
                          <a href="single-post.html">
                            10 Horrible Habits You're Doing Right Now That Are
                            Draining Your Energy
                          </a>
                        </h2>
                        <ul className="entry__meta">
                          <li className="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li className="entry__meta-date">Jan 21, 2018</li>
                        </ul>
                      </div>
                      <div className="entry__excerpt">
                        <p>
                          iPrice Group report offers insights on daily
                          e-commerce activity in the ...
                        </p>
                      </div>
                    </div>
                  </article>
                </div>
              </div>
            </div>{" "}
            {}
          </div>{" "}
          {}
        </section>{" "}
        {}
      </div>{" "}
      {}
      {}
      <SideBarRight></SideBarRight> {}
    </div>{" "}
    {}
    {}
    <AdBanner></AdBanner>
    {}
    <CarouselPosts></CarouselPosts> {}
    {}
    <PostCategory></PostCategory> {}
    {}
    <VideoPost></VideoPost> {}
    {}
    <div className="row">
      {}
      <WorldWidePost></WorldWidePost> {}
      {}
      <aside className="col-lg-4 sidebar sidebar--1 sidebar--right">
        {}
        <aside className="widget widget_media_image">
          <a href="#">
            <img src="img/content/placeholder_336.jpg" alt />
          </a>
        </aside>{" "}
        {}
        {}
        <aside className="widget widget_categories">
          <h4 className="widget-title">Categories</h4>
          <ul>
            <li>
              <a href="categories.html">
                World <span className="categories-count">5</span>
              </a>
            </li>
            <li>
              <a href="categories.html">
                Lifestyle <span className="categories-count">7</span>
              </a>
            </li>
            <li>
              <a href="categories.html">
                Business <span className="categories-count">5</span>
              </a>
            </li>
            <li>
              <a href="categories.html">
                Fashion <span className="categories-count">8</span>
              </a>
            </li>
            <li>
              <a href="categories.html">
                Investment <span className="categories-count">10</span>
              </a>
            </li>
            <li>
              <a href="categories.html">
                Technology <span className="categories-count">6</span>
              </a>
            </li>
          </ul>
        </aside>{" "}
        {}
        {}
        <aside className="widget widget-rating-posts">
          <h4 className="widget-title">Recommended</h4>
          <article className="entry">
            <div className="entry__img-holder">
              <a href="single-post.html">
                <div className="thumb-container thumb-60">
                  <img
                    data-src="img/content/review/review_post_1.jpg"
                    src="img/empty.png"
                    className="entry__img lazyload"
                    alt
                  />
                </div>
              </a>
            </div>
            <div className="entry__body">
              <div className="entry__header">
                <h2 className="entry__title">
                  <a href="single-post.html">
                    UN’s WFP Building Up Blockchain-Based Payments System
                  </a>
                </h2>
                <ul className="entry__meta">
                  <li className="entry__meta-author">
                    <span>by</span>
                    <a href="#">DeoThemes</a>
                  </li>
                  <li className="entry__meta-date">Jan 21, 2018</li>
                </ul>
                <ul className="entry__meta">
                  <li className="entry__meta-rating">
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star-empty" />
                  </li>
                </ul>
              </div>
            </div>
          </article>
          <article className="entry">
            <div className="entry__img-holder">
              <a href="single-post.html">
                <div className="thumb-container thumb-60">
                  <img
                    data-src="img/content/review/review_post_2.jpg"
                    src="img/empty.png"
                    className="entry__img lazyload"
                    alt
                  />
                </div>
              </a>
            </div>
            <div className="entry__body">
              <div className="entry__header">
                <h2 className="entry__title">
                  <a href="single-post.html">
                    4 credit card tips to make business travel easier
                  </a>
                </h2>
                <ul className="entry__meta">
                  <li className="entry__meta-author">
                    <span>by</span>
                    <a href="#">DeoThemes</a>
                  </li>
                  <li className="entry__meta-date">Jan 21, 2018</li>
                </ul>
                <ul className="entry__meta">
                  <li className="entry__meta-rating">
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star" />
                    {}
                    <i className="ui-star-empty" />
                  </li>
                </ul>
              </div>
            </div>
          </article>
        </aside>{" "}
        {}
      </aside>{" "}
      {}
    </div>{" "}
    {}
  </div>
);

export default LastNews;
