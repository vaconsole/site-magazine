import React from "react";

const CarouselPosts = () => (
  <section className="section mb-0">
    <div className="title-wrap title-wrap--line title-wrap--pr">
      <h3 className="section-title">editors picks</h3>
    </div>
    {}
    <div
      id="owl-posts"
      className="owl-carousel owl-theme owl-carousel--arrows-outside"
    >
      <article className="entry thumb thumb--size-1">
        <div
          className="entry__img-holder thumb__img-holder"
          style={{
            backgroundImage: 'url("img/content/carousel/carousel_post_1.jpg")'
          }}
        >
          <div className="bottom-gradient" />
          <div className="thumb-text-holder">
            <h2 className="thumb-entry-title">
              <a href="single-post.html">
                9 Things to Consider Before Accepting a New Job
              </a>
            </h2>
          </div>
          <a href="single-post.html" className="thumb-url" />
        </div>
      </article>
      <article className="entry thumb thumb--size-1">
        <div
          className="entry__img-holder thumb__img-holder"
          style={{
            backgroundImage: 'url("img/content/carousel/carousel_post_2.jpg")'
          }}
        >
          <div className="bottom-gradient" />
          <div className="thumb-text-holder">
            <h2 className="thumb-entry-title">
              <a href="single-post.html">
                Gov’t Toughens Rules to Ensure 3rd Telco Player Doesn’t Slack
                Off
              </a>
            </h2>
          </div>
          <a href="single-post.html" className="thumb-url" />
        </div>
      </article>
      <article className="entry thumb thumb--size-1">
        <div
          className="entry__img-holder thumb__img-holder"
          style={{
            backgroundImage: 'url("img/content/carousel/carousel_post_3.jpg")'
          }}
        >
          <div className="bottom-gradient" />
          <div className="thumb-text-holder">
            <h2 className="thumb-entry-title">
              <a href="single-post.html">
                (Infographic) Is Work-Life Balance Even Possible?
              </a>
            </h2>
          </div>
          <a href="single-post.html" className="thumb-url" />
        </div>
      </article>
      <article className="entry thumb thumb--size-1">
        <div
          className="entry__img-holder thumb__img-holder"
          style={{
            backgroundImage: 'url("img/content/carousel/carousel_post_4.jpg")'
          }}
        >
          <div className="bottom-gradient" />
          <div className="thumb-text-holder">
            <h2 className="thumb-entry-title">
              <a href="single-post.html">
                Is Uber Considering To Sell its Southeast Asia Business to Grab?
              </a>
            </h2>
          </div>
          <a href="single-post.html" className="thumb-url" />
        </div>
      </article>
      <article className="entry thumb thumb--size-1">
        <div
          className="entry__img-holder thumb__img-holder"
          style={{
            backgroundImage: 'url("img/content/carousel/carousel_post_2.jpg")'
          }}
        >
          <div className="bottom-gradient" />
          <div className="thumb-text-holder">
            <h2 className="thumb-entry-title">
              <a href="single-post.html">
                Gov’t Toughens Rules to Ensure 3rd Telco Player Doesn’t Slack
                Off
              </a>
            </h2>
          </div>
          <a href="single-post.html" className="thumb-url" />
        </div>
      </article>
    </div>{" "}
    {}
  </section>
);

export default CarouselPosts;
