import React from "react";

const SideBarRight = () => (
  <aside className="col-lg-4 sidebar sidebar--right">
    {}
    <aside className="widget widget-popular-posts">
      <h4 className="widget-title">Popular Posts</h4>
      <ul className="post-list-small">
        <li className="post-list-small__item">
          <article className="post-list-small__entry clearfix">
            <div className="post-list-small__img-holder">
              <div className="thumb-container thumb-100">
                <a href="single-post.html">
                  <img
                    data-src="img/content/post_small/post_small_1.jpg"
                    src="img/empty.png"
                    alt
                    className="post-list-small__img--rounded lazyload"
                  />
                </a>
              </div>
            </div>
            <div className="post-list-small__body">
              <h3 className="post-list-small__entry-title">
                <a href="single-post.html">
                  Follow These Smartphone Habits of Successful Entrepreneurs
                </a>
              </h3>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
          </article>
        </li>
        <li className="post-list-small__item">
          <article className="post-list-small__entry clearfix">
            <div className="post-list-small__img-holder">
              <div className="thumb-container thumb-100">
                <a href="single-post.html">
                  <img
                    data-src="img/content/post_small/post_small_2.jpg"
                    src="img/empty.png"
                    alt
                    className="post-list-small__img--rounded lazyload"
                  />
                </a>
              </div>
            </div>
            <div className="post-list-small__body">
              <h3 className="post-list-small__entry-title">
                <a href="single-post.html">
                  Lose These 12 Bad Habits If You're Serious About Becoming a
                  Millionaire
                </a>
              </h3>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
          </article>
        </li>
        <li className="post-list-small__item">
          <article className="post-list-small__entry clearfix">
            <div className="post-list-small__img-holder">
              <div className="thumb-container thumb-100">
                <a href="single-post.html">
                  <img
                    data-src="img/content/post_small/post_small_3.jpg"
                    src="img/empty.png"
                    alt
                    className="post-list-small__img--rounded lazyload"
                  />
                </a>
              </div>
            </div>
            <div className="post-list-small__body">
              <h3 className="post-list-small__entry-title">
                <a href="single-post.html">
                  June in Africa: Taxi wars, smarter cities and increased
                  investments
                </a>
              </h3>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
          </article>
        </li>
        <li className="post-list-small__item">
          <article className="post-list-small__entry clearfix">
            <div className="post-list-small__img-holder">
              <div className="thumb-container thumb-100">
                <a href="single-post.html">
                  <img
                    data-src="img/content/post_small/post_small_4.jpg"
                    src="img/empty.png"
                    alt
                    className="post-list-small__img--rounded lazyload"
                  />
                </a>
              </div>
            </div>
            <div className="post-list-small__body">
              <h3 className="post-list-small__entry-title">
                <a href="single-post.html">
                  PUBG Desert Map Finally Revealed, Here Are All The Details
                </a>
              </h3>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
          </article>
        </li>
      </ul>
    </aside>{" "}
    {}
    {}
    <aside className="widget widget_mc4wp_form_widget">
      <h4 className="widget-title">Newsletter</h4>
      <p className="newsletter__text">
        <i className="ui-email newsletter__icon" />
        Subscribe for our daily news
      </p>
      <form className="mc4wp-form" method="post">
        <div className="mc4wp-form-fields">
          <div className="form-group">
            <input
              type="email"
              name="EMAIL"
              placeholder="Your email"
              required
            />
          </div>
          <div className="form-group">
            <input
              type="submit"
              className="btn btn-lg btn-color"
              defaultValue="Sign Up"
            />
          </div>
        </div>
      </form>
    </aside>{" "}
    {}
    {}
    <aside className="widget widget-socials">
      <h4 className="widget-title">Let's hang out on social</h4>
      <div className="socials socials--wide socials--large">
        <div className="row row-16">
          <div className="col">
            <a
              className="social social-facebook"
              href="#"
              title="facebook"
              target="_blank"
              aria-label="facebook"
            >
              <i className="ui-facebook" />
              <span className="social__text">Facebook</span>
            </a>
            {}
            <a
              className="social social-twitter"
              href="#"
              title="twitter"
              target="_blank"
              aria-label="twitter"
            >
              <i className="ui-twitter" />
              <span className="social__text">Twitter</span>
            </a>
            {}
            <a
              className="social social-youtube"
              href="#"
              title="youtube"
              target="_blank"
              aria-label="youtube"
            >
              <i className="ui-youtube" />
              <span className="social__text">Youtube</span>
            </a>
          </div>
          <div className="col">
            <a
              className="social social-google-plus"
              href="#"
              title="google"
              target="_blank"
              aria-label="google"
            >
              <i className="ui-google" />
              <span className="social__text">Google+</span>
            </a>
            {}
            <a
              className="social social-instagram"
              href="#"
              title="instagram"
              target="_blank"
              aria-label="instagram"
            >
              <i className="ui-instagram" />
              <span className="social__text">Instagram</span>
            </a>
            {}
            <a
              className="social social-rss"
              href="#"
              title="rss"
              target="_blank"
              aria-label="rss"
            >
              <i className="ui-rss" />
              <span className="social__text">Rss</span>
            </a>
          </div>
        </div>
      </div>
    </aside>{" "}
    {}
  </aside>
);

export default SideBarRight;
