import React from "react";

const AdBanner = () => (
  <div className="text-center pb-48">
    <a href="#">
      <img src="img/content/placeholder_728.jpg" alt />
    </a>
  </div>
);

export default AdBanner;
