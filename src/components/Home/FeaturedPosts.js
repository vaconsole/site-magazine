import React from 'react'
import { Link } from 'gatsby'
import Image from 'gatsby-image'

const FeaturedPosts = ({ posts }) => (
  <section className="featured-posts-grid" featuredpost>
    <div className="container">
      <div className="row row-8">
        <div className="col-lg-6">
          {}
          <div className="featured-posts-grid__item featured-posts-grid__item--sm">
            <Link to={posts[1].node.fields.slug}>
              <article className="entry card post-list featured-posts-grid__entry">
                <div className="entry__img-holder post-list__img-holder card__img-holder">
                  {posts[1].node.frontmatter.cover && (
                    <Image
                      fluid={posts[1].node.frontmatter.cover.childImageSharp.fluid}
                      alt={posts[1].node.frontmatter.title}
                    />
                  )}
                  <a
                    href="categories.html"
                    className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
                  >
                    World
                  </a>
                </div>
                <div className="entry__body post-list__body card__body">
                  <h2 className="entry__title">{posts[1].node.frontmatter.title}</h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">{posts[1].node.frontmatter.date}</li>
                  </ul>
                </div>
              </article>
            </Link>
          </div>{' '}
          {}
          {}
          <div className="featured-posts-grid__item featured-posts-grid__item--sm">
            <Link to={posts[2].node.fields.slug}>
              <article className="entry card post-list featured-posts-grid__entry">
                <div
                  className="entry__img-holder post-list__img-holder card__img-holder"
                  style={{
                    backgroundImage: 'url(img/content/hero/hero_post_2.jpg)',
                  }}
                >
                  {posts[2].node.frontmatter.cover && (
                    <Image
                      fluid={posts[2].node.frontmatter.cover.childImageSharp.fluid}
                      alt={posts[2].node.frontmatter.title}
                    />
                  )}
                  <a
                    href="categories.html"
                    className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
                  >
                    Fashion
                  </a>
                </div>
                <div className="entry__body post-list__body card__body">
                  <h2 className="entry__title">{posts[2].node.frontmatter.title}</h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">{posts[2].node.frontmatter.date}</li>
                  </ul>
                </div>
              </article>
            </Link>
          </div>{' '}
          {}
          {}
          <div className="featured-posts-grid__item featured-posts-grid__item--sm">
            <Link to={posts[3].node.fields.slug}>
              <article className="entry card post-list featured-posts-grid__entry">
                <div
                  className="entry__img-holder post-list__img-holder card__img-holder"
                  style={{
                    backgroundImage: 'url(img/content/hero/hero_post_3.jpg)',
                  }}
                >
                  {posts[3].node.frontmatter.cover && (
                    <Image
                      fluid={posts[3].node.frontmatter.cover.childImageSharp.fluid}
                      alt={posts[3].node.frontmatter.title}
                    />
                  )}
                  <a
                    href="categories.html"
                    className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"
                  >
                    Business
                  </a>
                </div>
                <div className="entry__body post-list__body card__body">
                  <h2 className="entry__title">{posts[3].node.frontmatter.title}</h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">{posts[3].node.frontmatter.date}</li>
                  </ul>
                </div>
              </article>
            </Link>
          </div>{' '}
          {}
        </div>{' '}
        {}
        <div className="col-lg-6">
          {}
          <div className="featured-posts-grid__item featured-posts-grid__item--lg">
            <Link to={posts[0].node.fields.slug}>
              <article className="entry card featured-posts-grid__entry">
                <div className="entry__img-holder card__img-holder">
                  {posts[0].node.frontmatter.cover && (
                    <Image
                      fluid={posts[0].node.frontmatter.cover.childImageSharp.fluid}
                      alt={posts[0].node.frontmatter.title}
                    />
                  )}
                  <a
                    href="categories.html"
                    className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"
                  >
                    Lifestyle
                  </a>
                </div>
                <div className="entry__body card__body">
                  <h2 className="entry__title">{posts[0].node.frontmatter.title}</h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">{posts[0].node.frontmatter.date}</li>
                  </ul>
                </div>
              </article>
            </Link>
          </div>{' '}
          {}
        </div>
      </div>
    </div>
  </section>
)

export default FeaturedPosts
