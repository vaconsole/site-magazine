import React from "react";

const PostCategory = () => (
  <section className="section mb-0">
    <div className="row">
      {}
      <div className="col-md-6">
        <div className="title-wrap title-wrap--line">
          <h3 className="section-title">technology</h3>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <article className="entry thumb thumb--size-2">
              <div
                className="entry__img-holder thumb__img-holder"
                style={{
                  backgroundImage: 'url("img/content/thumb/thumb_post_1.jpg")'
                }}
              >
                <div className="bottom-gradient" />
                <div className="thumb-text-holder thumb-text-holder--1">
                  <h2 className="thumb-entry-title">
                    <a href="single-post.html">
                      Gov’t Toughens Rules to Ensure 3rd Telco Player Doesn’t
                      Slack Off
                    </a>
                  </h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">Jan 21, 2018</li>
                  </ul>
                </div>
                <a href="single-post.html" className="thumb-url" />
              </div>
            </article>
          </div>
          <div className="col-lg-6">
            <ul className="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Need a Website for Your Business? Google Can Help
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Here Are Ways You Can Earn From the Sharing Economy
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        19 Crazy Facts You Probably Didn't Know About Google
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        What Household Chores Would Filipinos Love to Assign to
                        Robots?
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
            </ul>
          </div>
        </div>
      </div>{" "}
      {}
      {}
      <div className="col-md-6">
        <div className="title-wrap title-wrap--line">
          <h3 className="section-title">travel</h3>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <article className="entry thumb thumb--size-2">
              <div
                className="entry__img-holder thumb__img-holder"
                style={{
                  backgroundImage: 'url("img/content/thumb/thumb_post_2.jpg")'
                }}
              >
                <div className="bottom-gradient" />
                <div className="thumb-text-holder thumb-text-holder--1">
                  <h2 className="thumb-entry-title">
                    <a href="single-post.html">
                      4 credit card tips to make business travel easier
                    </a>
                  </h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">Jan 21, 2018</li>
                  </ul>
                </div>
                <a href="single-post.html" className="thumb-url" />
              </div>
            </article>
          </div>
          <div className="col-lg-6">
            <ul className="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        5 deadliest luxury vacation spots on Earth
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        These 4 startups can send you to work and travel abroad
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        9 mind-blowing travel destinations for adventure seekers
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        These Small Business Ideas Are Great for Entrepreneurial
                        Children
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
            </ul>
          </div>
        </div>
      </div>{" "}
      {}
      {}
      <div className="col-md-6">
        <div className="title-wrap title-wrap--line">
          <h3 className="section-title">Cryptocurrency</h3>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <article className="entry thumb thumb--size-2">
              <div
                className="entry__img-holder thumb__img-holder"
                style={{
                  backgroundImage: 'url("img/content/thumb/thumb_post_3.jpg")'
                }}
              >
                <div className="bottom-gradient" />
                <div className="thumb-text-holder thumb-text-holder--1">
                  <h2 className="thumb-entry-title">
                    <a href="single-post.html">
                      UN’s WFP Building Up Blockchain-Based Payments System
                    </a>
                  </h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">Jan 21, 2018</li>
                  </ul>
                </div>
                <a href="single-post.html" className="thumb-url" />
              </div>
            </article>
          </div>
          <div className="col-lg-6">
            <ul className="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Cryptocurrency Markets: Weekly Trading Overview (06-13
                        October)
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        MyEtherWallet Opens the Ethereum Universe for You
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Put Your Money Where Your Marketing Is
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        3 ways for startups to master the art of emailing
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
            </ul>
          </div>
        </div>
      </div>{" "}
      {}
      {}
      <div className="col-md-6">
        <div className="title-wrap title-wrap--line">
          <h3 className="section-title">Investment</h3>
        </div>
        <div className="row">
          <div className="col-lg-6">
            <article className="entry thumb thumb--size-2">
              <div
                className="entry__img-holder thumb__img-holder"
                style={{
                  backgroundImage: 'url("img/content/thumb/thumb_post_4.jpg")'
                }}
              >
                <div className="bottom-gradient" />
                <div className="thumb-text-holder thumb-text-holder--1">
                  <h2 className="thumb-entry-title">
                    <a href="single-post.html">
                      14 easy, low-cost ways authors can promote their books
                    </a>
                  </h2>
                  <ul className="entry__meta">
                    <li className="entry__meta-author">
                      <span>by</span>
                      <a href="#">DeoThemes</a>
                    </li>
                    <li className="entry__meta-date">Jan 21, 2018</li>
                  </ul>
                </div>
                <a href="single-post.html" className="thumb-url" />
              </div>
            </article>
          </div>
          <div className="col-lg-6">
            <ul className="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Financial Adviser: 4 ways to know how much dividends you
                        should pay
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        3 cash flow rules first-time business owners need to
                        know
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Common credit mistakes new business owners make
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
              <li className="post-list-small__item">
                <article className="post-list-small__entry">
                  <div className="post-list-small__body">
                    <h3 className="post-list-small__entry-title">
                      <a href="single-post.html">
                        Ask these 2 questions every time you make a purchase
                      </a>
                    </h3>
                  </div>
                </article>
              </li>
            </ul>
          </div>
        </div>
      </div>{" "}
      {}
    </div>
  </section>
);

export default PostCategory;
