import React from "react";

const VideoPost = () => (
  <section className="section mb-24">
    <div className="title-wrap title-wrap--line">
      <h3 className="section-title">Videos</h3>
      <a href="#" className="all-posts-url">
        View All
      </a>
    </div>
    <div className="row card-row">
      <div className="col-md-6">
        <article className="entry card">
          <div className="entry__img-holder card__img-holder">
            <a href="single-post.html">
              <div className="thumb-container thumb-65">
                <img
                  data-src="img/content/grid/grid_post_5.jpg"
                  src="img/empty.png"
                  className="entry__img lazyload"
                  alt
                />
              </div>
            </a>
            <div className="entry__play-time">
              <i className="ui-play" />
              3:21
            </div>
          </div>
          <div className="entry__body card__body">
            <div className="entry__header">
              <h2 className="entry__title">
                <a href="single-post.html">
                  What Days and Hours are PH Online Shoppers Most Likely to Buy?
                </a>
              </h2>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
            <div className="entry__excerpt">
              <p>
                iPrice Group report offers insights on daily e-commerce activity
                in the ...
              </p>
            </div>
          </div>
        </article>
      </div>
      <div className="col-md-6">
        <article className="entry card">
          <div className="entry__img-holder card__img-holder">
            <a href="single-post.html">
              <div className="thumb-container thumb-65">
                <img
                  data-src="img/content/grid/grid_post_6.jpg"
                  src="img/empty.png"
                  className="entry__img lazyload"
                  alt
                />
              </div>
            </a>
            <div className="entry__play-time">
              <i className="ui-play" />
              3:21
            </div>
          </div>
          <div className="entry__body card__body">
            <div className="entry__header">
              <h2 className="entry__title">
                <a href="single-post.html">
                  Want to work at Tesla? This program guarantees you a job after
                  graduation
                </a>
              </h2>
              <ul className="entry__meta">
                <li className="entry__meta-author">
                  <span>by</span>
                  <a href="#">DeoThemes</a>
                </li>
                <li className="entry__meta-date">Jan 21, 2018</li>
              </ul>
            </div>
            <div className="entry__excerpt">
              <p>
                iPrice Group report offers insights on daily e-commerce activity
                in the ...
              </p>
            </div>
          </div>
        </article>
      </div>
    </div>
  </section>
);

export default VideoPost;
