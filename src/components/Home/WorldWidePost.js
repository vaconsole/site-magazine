import React from "react";

const WorldWidePost = () => (
  <div className="col-lg-8 blog__content mb-72">
    {}
    <section className="section">
      <div className="title-wrap title-wrap--line">
        <h3 className="section-title">Worldwide</h3>
        <a href="#" className="all-posts-url">
          View All
        </a>
      </div>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_1.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_1.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"
          >
            Business
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                These Are the 20 Best Places to Work in 2018
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_2.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_2.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"
          >
            Fashion
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                This Co-Working Space is a One-Stop Shop
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_3.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_3.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange"
          >
            Mining
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                This 6-Year-Old Kid Reportedly Earned US$11 Million on YouTube
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_4.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_4.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"
          >
            World
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                Rating Agency Breaks with Financial Community{" "}
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_5.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_5.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--red"
          >
            Investment
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                Decentralized Exchanges (DEX) – What Are They?
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
      <article className="entry card post-list">
        <div
          className="entry__img-holder post-list__img-holder card__img-holder"
          style={{
            backgroundImage: "url(img/content/list/list_post_6.jpg)"
          }}
        >
          <a href="single-post.html" className="thumb-url" />
          <img
            src="img/content/list/list_post_6.jpg"
            alt
            className="entry__img d-none"
          />
          <a
            href="categories.html"
            className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--cyan"
          >
            Technology
          </a>
        </div>
        <div className="entry__body post-list__body card__body">
          <div className="entry__header">
            <h2 className="entry__title">
              <a href="single-post.html">
                Decentralized Exchanges (DEX) – What Are They?
              </a>
            </h2>
            <ul className="entry__meta">
              <li className="entry__meta-author">
                <span>by</span>
                <a href="#">DeoThemes</a>
              </li>
              <li className="entry__meta-date">Jan 21, 2018</li>
            </ul>
          </div>
          <div className="entry__excerpt">
            <p>
              iPrice Group report offers insights on daily e-commerce activity
              in the ...
            </p>
          </div>
        </div>
      </article>
    </section>{" "}
    {}
    {}
    <nav className="pagination">
      <span className="pagination__page pagination__page--current">1</span>
      <a href="#" className="pagination__page">
        2
      </a>
      <a href="#" className="pagination__page">
        3
      </a>
      <a href="#" className="pagination__page">
        4
      </a>
      <a
        href="#"
        className="pagination__page pagination__icon pagination__page--next"
      >
        <i className="ui-arrow-right" />
      </a>
    </nav>
  </div>
);

export default WorldWidePost;
