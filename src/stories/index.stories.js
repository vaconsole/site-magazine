import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import { Button, Welcome } from '@storybook/react/demo'
import Navbar from '../components/navbar'

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />)

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ))

storiesOf(`Navbar`, module).add(`Navbar, no logo, seo styleing`, () => {
  const mainMenu = [
    {
      name: 'home',
      link: '/',
    },
    {
      name: 'blogs',
      link: '/category/blog',
    },
  ]
  return (
    <Navbar menu={mainMenu} className="header header-sticky">
      <h2>hello world</h2>
    </Navbar>
  )
})
