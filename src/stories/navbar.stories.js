import React from 'react'
import { storiesOf } from '@storybook/react'
import MainNavBar from '../components/MainNavBar'
import NavBar from '../components/navbar'

storiesOf(`Navbar`, module).add(`Navbar With Static Query`, () => <MainNavBar />)

storiesOf(`Navbar`, module).add(`Navbar, no logo`, () => {
  const mainMenu = [
    {
      name: 'home',
      link: '/',
    },
    {
      name: 'blogs',
      link: '/category/blog',
    },
  ]
  return (
    <NavBar menu={mainMenu} className="navbar-dark bg-dark">
      <h2>hello world</h2>
    </NavBar>
  )
})

storiesOf(`Navbar`, module).add(`Navbar, no menu`, () => {
  const data = {
    site: {
      siteMetadata: {
        short_name: 'Neterial.com',
      },
    },
    logo: null,
  }

  return (
    <NavBar>
      <h2>hello </h2>
    </NavBar>
  )
})
