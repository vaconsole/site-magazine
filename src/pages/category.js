import React from 'react'
import Layout from '../components/Global/Layout'
import Category from '../components/Category/CategorySection'
import BreadCrumbs from '../components/Global/BreadCrumbs'

const Index = () => (
  <Layout>
    <BreadCrumbs />
    <Category />
  </Layout>
)

export default Index
