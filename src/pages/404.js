import React from 'react'
import Layout from '../components/Global/Layout'
import Error404 from '../components/Error404'

const Index = () => (
  <Layout>
    <Error404 />
  </Layout>
)

export default Index
