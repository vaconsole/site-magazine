import React from 'react'
import Layout from '../components/Global/Layout'
import About from '../components/AboutSection'
import BreadCrumbs from '../components/Global/BreadCrumbs'

const Index = () => (
  <Layout>
    <BreadCrumbs />
    <About />
  </Layout>
)

export default Index
