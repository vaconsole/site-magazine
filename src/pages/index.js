import React from 'react'
import { graphql } from 'gatsby'
import NewsFlash from '../components/Home/NewsFlash'
import FeaturedPosts from '../components/Home/FeaturedPosts'
import LastNews from '../components/Home/LastNews'
import Layout from '../components/Global/Layout'

const Home = ({ data }) => {
  const posts = data.posts.edges
  return (
    <Layout>
      {}
      {}
      <NewsFlash />
      {}
      <FeaturedPosts posts={posts.slice(0, 4)} /> {}
      <LastNews /> {}
    </Layout>
  )
}

export default Home

export const pageQuery = graphql`
  query HomePage {
    posts: allMarkdownRemark(filter: { frontmatter: { categories: { in: "blog" } } }) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            tags
            templateKey
            categories
            date(formatString: "DD/MM/YYYY")
            cover {
              childImageSharp {
                fluid(quality: 90, maxWidth: 4160) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`
