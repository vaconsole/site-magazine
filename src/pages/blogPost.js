import React from 'react'
import Layout from '../components/Global/Layout'
import BreadCrumbs from '../components/Global/BreadCrumbs'
import BannerImage from '../components/BlogPost/BannerImage'
import BlogSection from '../components/BlogPost/BlogSection'

const Home = () => (
  <Layout>
    <BreadCrumbs />
    <BannerImage />
    <BlogSection />
  </Layout>
)

export default Home
