import React from 'react'
import Layout from '../components/Global/Layout'
import Contact from '../components/Contact'
import BreadCrumbs from '../components/Global/BreadCrumbs'

const Index = () => (
  <Layout>
    <BreadCrumbs />
    <Contact />
  </Layout>
)

export default Index
