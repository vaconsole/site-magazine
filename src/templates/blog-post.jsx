import React from 'react'
import Layout from '../components/Global/Layout'
import BreadCrumbs from '../components/Global/BreadCrumbs'
import BannerImage from '../components/BlogPost/BannerImage'
import SideBar from '../components/SideBar/SideBar'
import NewsLetter from '../components/SideBar/NewsLetter'
import SocialIcons from '../components/SideBar/SocialIcons'
import Review from '../components/SideBar/Review'
import { Link, graphql } from 'gatsby'
import NewsLetterWide from '../components/BlogPost/NewsLetterWide'
import Author from '../components/BlogPost/Author'
import RelatedArticle from '../components/BlogPost/RelatedArticle'
import SocialShare from '../components/BlogPost/SocialShare'
import FinalReview from '../components/BlogPost/FinalReview'
import { DiscussionEmbed, CommentCount } from "disqus-react";

const BlogPost = ({ data, pageContext, location }) => {

  const post = data.post
  const { previous, next } = pageContext
  const tags = post.frontmatter.tags
  const categories = post.frontmatter.categories
  const coverImg = post.frontmatter.cover && post.frontmatter.cover.childImageSharp.fluid

  const disqusShortname = data.site.siteMetadata.disqus_shortname;
  const disqusConfig = {
    identifier: post.id,
    title: post.frontmatter.title,
    url: `${data.site.siteMetadata.siteUrl}${location.pathname}`
  };

  return (
    <Layout>
      <BreadCrumbs />
      <BannerImage img={coverImg} />
      <div className="main-container container" id="main-container">
        {}
        <div className="row">

          <div className="col-lg-8 blog__content mb-72">
            <div className="content-box content-box--top-offset">
              {}
              <article className="entry mb-0">
                <div className="single-post__entry-header entry__header">
                  <ul className="entry__meta">
                    {categories &&
                      categories.map(category => (
                        <Link className="entry__meta-category" key={category} to={`/category/${category}`}>
                          {category}
                        </Link>
                      ))}
                  </ul>
                  <h1 className="single-post__entry-title">{post.title}</h1>
                  <div className="entry__meta-holder">
                    <ul className="entry__meta">
                      <li className="entry__meta-author">
                        <span>by</span>
                        <a href="#">DeoThemes</a>
                      </li>
                    </ul>
                    <ul className="entry__meta">
                      {/* <li className="entry__meta-views">
                        <i className="ui-eye" />
                        <span>1356</span>
                      </li> */}
                      <li className="entry__meta-comments">
                        <a href="#">
                          <i className="ui-chat-empty" />
                          <CommentCount shortname={disqusShortname} config={disqusConfig} />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>{' '}
                {}
                <div className="entry__article-wrap">
                  {}
                  <SocialShare /> {}
                  <div className="entry__article">
                    <div dangerouslySetInnerHTML={{ __html: post.html }} />
                    <FinalReview /> {}
                    {}
                    <div className="entry__tags">
                      <i className="ui-tags" />
                      <span className="entry__tags-label">Tags:</span>
                      {tags &&
                        tags.map(tag => (
                          <Link key={tag} to={`/tag/${tag}`}>
                            {tag}
                          </Link>
                        ))}
                    </div>{' '}
                  </div>{' '}
                  {}
                </div>{' '}
                {}
                {}
                <NewsLetterWide /> {}
                {}
                <nav className="entry-navigation">
                  <div className="clearfix">
                    <div className="entry-navigation--left">
                      <i className="ui-arrow-left" />
                      <span className="entry-navigation__label">Previous Post</span>
                      <div className="entry-navigation__link">
                        {next && (
                          <Link to={next.fields.slug} rel="next">
                            {next.frontmatter.title}
                          </Link>
                        )}
                      </div>
                    </div>
                    <div className="entry-navigation--right">
                      <span className="entry-navigation__label">Next Post</span>
                      <i className="ui-arrow-right" />
                      <div className="entry-navigation__link">
                        {previous && (
                          <Link to={previous.fields.slug} rel="prev">
                            ← {previous.frontmatter.title}
                          </Link>
                        )}
                      </div>
                    </div>
                  </div>
                </nav>                {}
                <Author />
                {}
                <RelatedArticle /> {}
              </article>{' '}
              {}
              {}
              <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />

            </div>{' '}
            {}
          </div>


          {}
          <SideBar>
            <Review /> {}
            {}
            <NewsLetter /> {}
            {}
            <SocialIcons /> {}
          </SideBar>
        </div>{' '}
        {}
      </div>    </Layout>)
}

export default BlogPost




export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
        disqus_shortname
        author
      }
    }
    post:markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 400)
      html
      frontmatter {
        title
        tags
        categories
        date(formatString: "MMMM DD, YYYY")
        description
        cover {
          childImageSharp {
           fluid(quality: 90, maxWidth: 4160) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  }
`

