import React from "react";

const BlogList = ({ data, pageContext }) => {

  const postEdges = data.allMarkdownRemark.edges
  return (
    <div />
  )
}

export default BlogList;


export const pageQuery = graphql`
query categories($taxonomyValue:String) {
    allMarkdownRemark(limit: 1000, filter: { frontmatter: { categories: { in: [$taxonomyValue] } } }) {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          excerpt
          timeToRead
          frontmatter {
            title
            tags
            cover {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            date
          }
        }
      }
    }
  }
`


