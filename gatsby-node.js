const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const _ = require('lodash')

const tagPage = path.resolve('src/templates/category.jsx')
const categoryPage = path.resolve('src/templates/category.jsx')

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const results = await graphql(
    `
      {
        allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }, limit: 1000) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                templateKey
                tags
                categories
              }
            }
          }
        }
      }
    `
  )

  // Create blog posts pages.
  const posts = results.data.allMarkdownRemark.edges
  const tagSet = new Set()
  const categorySet = new Set()
  posts.forEach((post, index) => {
    const previous = index === posts.length - 1 ? null : posts[index + 1].node
    const next = index === 0 ? null : posts[index - 1].node
    if (post.node.frontmatter.tags) {
      post.node.frontmatter.tags.forEach(tag => {
        tagSet.add(tag)
      })
    }

    if (post.node.frontmatter.categories) {
      post.node.frontmatter.categories.forEach(category => {
        categorySet.add(category)
      })
    }

    createPage({
      path: post.node.fields.slug,
      component: path.resolve(`src/templates/${String(post.node.frontmatter.templateKey)}.jsx`),
      context: {
        slug: post.node.fields.slug,
        previous,
        next,
      },
    })
  })

  // create tag pages
  tagSet.forEach(tag => {
    createPage({
      path: `/tag/${_.kebabCase(tag)}/`,
      component: tagPage,
      context: {
        tag,
      },
    })
  })

  categorySet.forEach(category => {
    createPage({
      path: `/category/${_.kebabCase(category)}/`,
      component: categoryPage,
      context: {
        taxonomyType: 'categories',
        taxonomyValue: category,
        category,
      },
    })
  })
  return null
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
